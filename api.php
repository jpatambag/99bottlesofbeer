<?php
$request_method = $_SERVER['REQUEST_METHOD'];
switch($request_method)
	{
		case 'GET':
			get_lyrics();
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
    }
 // GET LYRICS OBJECT
  function get_lyrics() {
    $response=array();
    for($i=99; $i>0; $i--) {
        $x = $i-1;
        $bottle_phrase = $i > 1 ? "bottles" : "bottle";
        $no_more_bottles = "no more bottles of beer on the wall";
        $bottle_of_beer = $bottle_phrase." of beer on the wall.";
        $take_phrase = $i > 1 ? " Take one down and pass it around,".' '.$x.' '.$bottle_of_beer : " Take it down and pass it around, ".$no_more_bottles;
        $fullString = $i.' '.$bottle_phrase.' '."of beer on the wall,".' '.$i.' '.$bottle_phrase.' '."of beer.".$take_phrase;
        array_push($response, $fullString);
        if ($i == 1) {
           array_push($response, ucfirst(strtolower($no_more_bottles)).", no more bottles of beer. Go to the store and buy some more, 99 bottles of beer on the wall.");
        }
    }
    header('Content-Type: application/json');
	echo json_encode($response);
  }  
?>