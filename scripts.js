const app = document.getElementById('root');

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(container);

var request = new XMLHttpRequest();
request.open('GET', 'http://localhost:8888/99Beers/api.php', true);
request.onload = function () {

    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {

        const card = document.createElement('div');
        card.setAttribute('class', 'card');
        container.appendChild(card);

        const h1 = document.createElement('h1');
        h1.textContent = "99 Bottles of Beer on the Wall.";
        card.appendChild(h1);

        data.forEach(content => {
            const p = document.createElement('p');
            p.textContent = `${content}`;
            card.appendChild(p);
        });
    } else {
        const errorMessage = document.createElement('marquee');
        errorMessage.textContent = `Error fetching from resource! `;
        app.appendChild(errorMessage);
    }
}

request.send();